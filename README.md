# Reign Hackers News

The project consists in a web page who render a lists of articles/news. This articles are taken out from https://hn.algolia.com/api/v1/search_by_date?query=nodejs and saved in a mongo data base. The client visit the articles and delete them.

Technology used:
- React: for the construction of app client side.
- Nestsj: for the construction of app api.
- Mongo: The data base of the app
- Docker: to create containers to display the app on another systems
- Jest: for the test

## Run the project: Server Side

Create the .env in to hacker-news-api folder with the next format:
```bash
MONGO_CONNECTION_URI='Mongo uri'
```

Now, open an terminal and run mongodb with:
```bash
mongod
```
With db on, in to api folder run the next commands:
```bash
npm install 
npm run start:dev
```
## Run the project: Client Side

Create the .env in to hacker-news folder with the next format:
```bash
REACT_APP_API=http://localhost:3001/news/
```
In the console, run the nexts commands:
```bash
npm install
npm run start
```
## Run the test: Server Side

for the unit test
```bash
npm run test
```
for the e2e test
```bash
npm run test:e2e
```
