import axios from "axios";

export const deleteNews = async (id) => {
  const response = await axios.delete(`${process.env.REACT_APP_API}${id}`);
  return response;
}