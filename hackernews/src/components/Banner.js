export const Banner = () => {
    return (
        <div className="container">
            <div className="box">
                <h1 className="title">HN Feed</h1>
                <h4>We {'<3'} hacker news!</h4>
            </div>
        </div>
    )
}