import { useEffect } from "react";
import { useNews } from "../hooks/useNews";
import { News } from "./News";

export const NewsList = () => {

    const {
        getN,
        news
    } = useNews();

    useEffect(() => {
        getN();
    },[getN])

    return (
        <ul className="list">
            {
            news.length === 0 ? 
            <div>We have 0 news to show :(</div> :
            news.map((item, index) => {
                return (
                <li key={index} className="">
                    <News news={item}></News>
                </li>)
            })
            }
        </ul>
    )
}