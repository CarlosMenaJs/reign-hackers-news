import { useNews } from "../hooks/useNews";

export const News = ({news}) => {
  
  const {
    DateToTime,
    deleteN
  } = useNews();

  const handleClick = async (event,id) => {
      event.preventDefault();
      const response = await deleteN(id);
      console.log(response);
  }

  return (
    <div className="list_item">
      <a className="list_link" href={news.url} target='_blank' rel="noreferrer">
          <div className="list_text">
              <div className="item_title">{ news.title }</div>
              <div className="item_author">- { news.author} -</div>
          </div>
          <div className="item_date">{DateToTime(news.date)}</div>
      </a>
          <div onClick={(event) => {handleClick(event,news._id)}} className="item_buttom">
              <i class="fas fa-trash-alt"></i>
          </div>
    </div>
  )
}