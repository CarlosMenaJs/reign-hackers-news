import './App.css';
import { Banner } from './components/Banner';
import { NewsList } from './components/NewsList';

function App() {
  return (
    <div className="">
      <Banner></Banner>
      <NewsList></NewsList>
    </div>
  );
}

export default App;
