import { useState } from "react";
import { getNews } from "../services/getNews";
import { deleteNews } from "../services/deleteNews";

export const useNews = () => {
    const [news, setNews] = useState([]);

    const deleteN = async (id) => {
        const response = await deleteNews(id);
        return response.data;
    }

    async function getN() {
        const response = await getNews();
        setNews(response);
    }

    const DateToTime = (created_at) => {
        const date = new Date(created_at);
        const currentDate = new Date();
        const isTheSameDay = date.getDate() === currentDate.getDate();
        const isTheSameMonth = date.getMonth() === currentDate.getMonth();
        const isTheSameYear = date.getFullYear() === currentDate.getFullYear();
        const isYesterday = date.getDate() - currentDate.getDate() === -1;
        if (isTheSameDay && isTheSameMonth && isTheSameYear) {
            return date.toLocaleTimeString('en', {
                hour: '2-digit',
                minute: '2-digit'
            });
        }else if (isTheSameMonth && isTheSameYear && isYesterday) {
            return 'Yesterday';
        }else if (isTheSameYear) {
            return date.toLocaleString('en', {
                day : '2-digit', 
                month : 'short' 
            }); 
        }else {
            return date.toLocaleString('en', {
                day : '2-digit', 
                month : 'short',
                year : '2-digit' 
            });
        }
    }

    return {
        getN,
        DateToTime,
        deleteN,
        news
    }
}