import {
  CACHE_MANAGER,
  Inject,
  Injectable,
  OnApplicationBootstrap,
} from '@nestjs/common';
import { Cache } from 'cache-manager';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Interval } from '@nestjs/schedule';
import { lastValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios';
import { HackerNews } from './interfaces/news.interface';
import { CreateNewsDTO } from './dto/news.dto';
import { NewsUtils } from '../utils/news.utils';
@Injectable()
export class NewsService implements OnApplicationBootstrap {

  constructor(
    @InjectModel('News') private readonly newsModel: Model<HackerNews>,
    private readonly httpService: HttpService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    private readonly newsUtils: NewsUtils,
  ) {}

  async onApplicationBootstrap() {
    //When the app starts, get the news of the db
    const currentNews = await this.getNews();
    //If the db is empty
    if (currentNews.length === 0) {
      //save all news form the external api
      await this.SetAllNews();
    } else { // if db have news
      //get the date of the last news saved and set as lastUpdate in cache
      await this.setLastUpdateNotEmpty();
      //Update the data base
      await this.UpdateDataBase();
    }
    //set the current date as lastUpdate in cache
    await this.cacheManager.set('lastUpdate', new Date());
  }

  //with interval the function execute every 3600000 ms (every 1 hour pass)
  @Interval(3600000)
  async UpdateDataBase(): Promise<HackerNews[]> {
    //get the news from the external api with axios
    const newSavedNews = [];
    const response = await lastValueFrom(
      this.httpService.get(
        'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      ),
    );
    const entryNews = response.data.hits;
    //get the news from the db
    const savedNews = await this.newsModel
      .find({})
      .sort({ date: 'desc' })
      .lean()
      .exec();
    //gets the last time the app updated the db
    let lastUpdate: Date = await this.cacheManager.get('lastUpdate');
    
    entryNews
      .filter((item) => {
        //Check if the news has the necessary data and is not already saved in the db
        if (this.newsUtils.isAValidNews(item) && !this.newsUtils.isSaved(item, savedNews)) {
          return item;
        }
      })
      .map(async (item) => {
        //whit the valid news create a new createNewsDto with the correct format
        const createNewsDTO: CreateNewsDTO = {
          title: item.story_title || item.title,
          url: item.url || item.story_url,
          author: item.author,
          date: item.created_at,
        };
        //compares the incoming news Date with the last time when the api did an update
        const newsDate = new Date(createNewsDTO.date);
        const isNew = newsDate > lastUpdate;
        //if the date of createNewsDTO pass lastUpdate
        //that means is a new news 
        if (isNew) {
          //create the model and save
          const newNews = new this.newsModel(createNewsDTO);
          await newNews.save();
          newSavedNews.push(newNews);
        }
      });
    //set the current date as lastUpdate in cache
    await this.cacheManager.set('lastUpdate', new Date());
    return newSavedNews;
  }
  //get all news saved in the db
  async getNews(): Promise<HackerNews[]> {
    const news = await this.newsModel.find().sort({ date: 'desc' }).exec();
    return news;
  }
  //Save all news from the external api in the db
  async SetAllNews(): Promise<HackerNews[]> {
    const response = await lastValueFrom(
      this.httpService.get(
        'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      ),
    );
    const entryNews = response.data.hits;
    entryNews
      .filter((item) => {
        if (this.newsUtils.isAValidNews(item)) {
          return item;
        }
      })
      .map(async (item) => {
        const createNewsDTO: CreateNewsDTO = {
          title: item.story_title || item.title,
          url: item.url || item.story_url,
          author: item.author,
          date: item.created_at,
        };
        const newNews = new this.newsModel(createNewsDTO);
        await newNews.save();
      });
    return entryNews;
  }
  //Delete one news by id
  async DeleteOneNews(id: string): Promise<HackerNews> {
    const newsDeleted = await this.newsModel.findByIdAndDelete(id);
    return newsDeleted;
  }

  //get the date of the last news in the db and set as lastUpdate
  async setLastUpdateNotEmpty(): Promise<void> {
    const news = await this.newsModel.find().sort({ date: 'desc' }).exec();
    const recentNews = new Date(news[0].date);
    await this.cacheManager.set('lastUpdate', recentNews);
  }
}
