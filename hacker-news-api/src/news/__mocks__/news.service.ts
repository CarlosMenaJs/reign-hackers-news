import { newsStub } from '../test/stubs/news.stub';

export const NewsService = jest.fn().mockReturnValue({
  getNews: jest.fn().mockResolvedValue([newsStub()]),
  DeleteOneNews: jest.fn().mockResolvedValue(newsStub()),
  UpdateDataBase: jest.fn().mockResolvedValue([newsStub]),
});
