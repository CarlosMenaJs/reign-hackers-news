import {
  Controller,
  Get,
  Delete,
  Param,
} from '@nestjs/common';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {
  constructor(private newsService: NewsService) {}

  @Get('/')
  async getAll() {
    return await this.newsService.getNews();
  }

  @Delete('/:id')
  async deleteOne(@Param('id') id: string) {
    return await this.newsService.DeleteOneNews(id);
  }
}
