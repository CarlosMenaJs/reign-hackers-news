import { HttpModule, HttpService } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsUtils } from '../utils/news.utils';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { NewsSchema } from './schemas/news.schema';
import { CacheModule } from '@nestjs/common';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'News', schema: NewsSchema }]),
    HttpModule,
    CacheModule.register({
      ttl: 0
    }),
  ],
  controllers: [NewsController],
  providers: [NewsService, NewsUtils],
})
export class NewsModule {}
