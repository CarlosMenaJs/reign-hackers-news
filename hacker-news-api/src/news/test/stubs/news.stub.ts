import { HackerNews } from 'src/news/interfaces/news.interface';

export const newsStub = (): HackerNews => {
  return {
    _id: '123',
    title: 'The best testing news',
    author: 'testor 3000',
    url: 'http://test.com',
    date: '01/01/2022',
  };
};
