import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { News } from '../schemas/news.schema';
import { Model } from 'mongoose';
import { NewsService } from '../news.service';
import { NewsModule } from '../news.module';
import { AppModule } from 'src/app.module';

const mockNews = {
  _id: '1',
  title: 'The best testing news',
  author: 'testor 3000',
  url: 'http://test.com',
  date: '01/01/2022',
};

describe('NewsService', () => {
  let newsService: NewsService;
  let model: Model<News>;

  const newsArray = [
    {
      _id: '1',
      title: 'The best testing news',
      author: 'testor 3000',
      url: 'http://test.com',
      date: '01/01/2022',
    },
    {
      _id: '2',
      title: 'The second best testing news',
      author: 'testor 2000',
      url: 'http://test2.com',
      date: '05/01/2022',
    },
    {
      _id: '3',
      title: 'The third best testing news',
      author: 'testor 3000',
      url: 'http://test.com',
      date: '01/01/2022',
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        {
          provide: getModelToken('News'),
          useValue: {
            new: jest.fn().mockResolvedValue(mockNews),
            constructor: jest.fn().mockResolvedValue(mockNews),
            find: jest.fn(),
            create: jest.fn(),
            exec: jest.fn(),
          },
        },
      ],
    }).compile();

    newsService = module.get<NewsService>(NewsService);
    model = module.get<Model<News>>(getModelToken('News'));
  });

  it('should return all news', async () => {
    jest.spyOn(model, 'find').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(newsArray),
    } as any);
    const news = await newsService.getNews();
    expect(news).toEqual(newsArray);
  });
});
