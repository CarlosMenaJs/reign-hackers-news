import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { NewsModule } from '../news.module';
import { NewsService } from '../news.service';
import { INestApplication } from '@nestjs/common';
import { newsStub } from './stubs/news.stub';

describe('News', () => {
  let app: INestApplication;
  const newsService = {
    getData: () => [newsStub()],
    deleteOneData: () => newsStub(),
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [NewsModule],
    })
      .overrideProvider(NewsService)
      .useValue(newsService)
      .compile();
    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('/GET news', () => {
    return request(app.getHttpServer()).get('/').expect(200).expect({
      data: newsService.getData(),
    });
  });

  it('/Delete news', () => {
    return request(app.getHttpServer()).delete('/news/123').expect(200).expect({
      data: newsStub(),
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
