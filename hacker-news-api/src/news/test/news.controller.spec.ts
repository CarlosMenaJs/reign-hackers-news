import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from '../news.controller';
import { NewsService } from '../news.service';
import { HackerNews } from '../interfaces/news.interface';
import { newsStub } from './stubs/news.stub';
import { CreateNewsDTO } from '../dto/news.dto';
import { HttpModule, HttpService } from '@nestjs/axios';

describe('NewsController', () => {
  let newsController: NewsController;
  let newsService: NewsService;

  const createNewsDto: CreateNewsDTO = {
    title: 'The best testing news',
    author: 'testor 3000',
    url: 'http://test.com',
    date: new Date('01/01/2022'),
  };

  const mockNews = {
    _id: '1',
    title: 'The best testing news',
    author: 'testor 3000',
    url: 'http://test.com',
    date: '01/01/2022',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [NewsController],
      providers: [
        {
          provide: NewsService,
          useValue: {
            getData: jest.fn().mockResolvedValue([
              {
                _id: '1',
                title: 'The best testing news',
                author: 'testor 3000',
                url: 'http://test.com',
                date: '01/01/2022',
              },
              {
                _id: '2',
                title: 'The second best testing news',
                author: 'testor 2000',
                url: 'http://test2.com',
                date: '05/01/2022',
              },
              {
                _id: '3',
                title: 'The third best testing news',
                author: 'testor 3000',
                url: 'http://test.com',
                date: '01/01/2022',
              },
            ]),
            deleteOneData: jest.fn().mockResolvedValue(mockNews),
          },
        },
      ],
    }).compile();

    newsController = module.get<NewsController>(NewsController);
    newsService = module.get<NewsService>(NewsService);
  });

  describe('getAll', () => {
    it('should return an array of news', () => {
      expect(newsController.getAll()).resolves.toEqual([
        {
          _id: '1',
          title: 'The best testing news',
          author: 'testor 3000',
          url: 'http://test.com',
          date: '01/01/2022',
        },
        {
          _id: '2',
          title: 'The second best testing news',
          author: 'testor 2000',
          url: 'http://test2.com',
          date: '05/01/2022',
        },
        {
          _id: '3',
          title: 'The third best testing news',
          author: 'testor 3000',
          url: 'http://test.com',
          date: '01/01/2022',
        },
      ]);
      expect(newsService.getNews).toHaveBeenCalled();
    });
  });

  describe('DeleteOne', () => {
    it('should return the deleted news', async () => {
      const deleteSpy = jest
        .spyOn(newsService, 'DeleteOneNews')
        .mockResolvedValueOnce(mockNews);
      expect(await newsController.deleteOne(mockNews._id)).toEqual(mockNews);
      expect(deleteSpy).toHaveBeenCalledWith(mockNews._id);
    });
  });
});
