export class CreateNewsDTO {
  readonly title: string;
  readonly author: string;
  readonly date: Date;
  readonly url: string;
}
