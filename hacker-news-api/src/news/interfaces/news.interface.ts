export interface HackerNews {
  _id: string;
  title: string;
  author: string;
  url: string;
  date: string;
}
