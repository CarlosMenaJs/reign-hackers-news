import { CACHE_MANAGER, Inject } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { HackerNews } from 'src/news/interfaces/news.interface';
export class NewsUtils {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  
  isAValidNews(news): boolean {
    const hasTitle = news.story_title || news.title;
    const hasUrl = news.url || news.story_url;
    const hasDate = news.created_at !== null;
    return hasTitle && hasUrl && hasDate;
  }

  isSaved(news, newsArray): boolean {
    return newsArray.some(
      (newsSaved) =>
        newsSaved.title === news.title || newsSaved.title === news.story_title,
    );
  }
}
